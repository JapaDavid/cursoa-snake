var escala = 20;
var cobrinha = snake();

function setup() {
  createCanvas(600, 600);
  frameRate(10);
  background("#F0F0FF");
}

function draw() {
  background("#F0F0FF");
  cobrinha.movimentar();
}

function snake() {
  this.x = 0;
  this.y = 0;
  this.largura = escala;
  this.altura = escala;
  this.velocidade = 1;
  this.multiplicadorX = 0;
  this.multiplicadorY = 0;

  this.movimentar = function () {
    this.x = this.x + (this.velocidade * this.multiplicadorX * escala);
    this.y = this.y + (this.velocidade * this.multiplicadorY * escala);
    this.desenhar();
  }

  this.alterarDirecao = function (keyCode) {
    switch (keyCode) {
      case UP_ARROW:
        this.multiplicadorX = 0;
        this.multiplicadorY = -1;
        break;
      case DOWN_ARROW:
        this.multiplicadorX = 0;
        this.multiplicadorY = 1;
        break;
      case LEFT_ARROW:
        this.multiplicadorX = -1;
        this.multiplicadorY = 0;
        break;
      case RIGHT_ARROW:
        this.multiplicadorX = 1;
        this.multiplicadorY = 0;
        break;
    }
  }

  this.desenhar = function () {
    noStroke();
    fill("#33FF00");
    rect(this.x, this.y, this.largura, this.altura);
  }

  return this;
}

function keyPressed() {
  cobrinha.alterarDirecao(keyCode);
}